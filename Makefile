# vim: syntax=make noexpandtab
CWD	=$(shell pwd)


env:
	virtualenv $(CWD)/env
	chmod +x $(CWD)/env/bin/activate
	bash -c ". $(CWD)/env/bin/activate \
		&& pip3 install -r requirements.txt \
		&& pip install -r requirements.txt \
		&& pip3 install -r requirements-testing.txt \
		&& pip install -r requirements-testing.txt \
		&& pip3 install -r requirements-dev.txt \
		&& pip install -r requirements-dev.txt \
		|| echo \"Install failed\""
