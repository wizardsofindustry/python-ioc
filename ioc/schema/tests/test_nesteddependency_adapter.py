import copy
import unittest

import marshmallow

from ioc.schema.adapters import NestedDependencyAdapter
from ioc.schema.dependency import NestedDependency
from ioc.schema.requirement import SchemaRequirement


class NestedDependencyAdapterTestCase(unittest.TestCase):

    def setUp(self):
        self.schema = NestedDependencyAdapter()

    def test_factory_is_required(self):
        params = {
            "name" : "foo",
        }
        dep, errors = self.schema.load(params)
        self.assertIn('factory', errors)

    def test_factory_must_be_callable_dependency(self):
        params = {
            "name" : "foo",
            "factory": None
        }
        dep, errors = self.schema.load(params)
        self.assertIn('factory', errors)

    def test_load_returns_nested_dependency(self):
        params = {
            "name" : "foo",
            "factory": {"type":"symbol", "value": "str"}
        }
        dep, errors = self.schema.load(params)
        self.assertIsInstance(dep, NestedDependency)
