import marshmallow
import six

from ioc import loader
from ioc import provider
from ioc.schema.requirement import SchemaRequirement
from ioc.schema.dependency import LiteralDependency
from ioc.schema.dependency import SimpleDependency
from ioc.schema.dependency import NestedDependency
from ioc.schema.dependency import DependencyCollection
from ioc.utils import is_valid_identifier


class BaseDependencyAdapter(marshmallow.Schema):

    @marshmallow.post_load
    def initialize_dependency(self, params):
        return SchemaRequirement(**params)

    @marshmallow.validates_schema
    def validate_value(self, params):
        if params.get('type') not in ('symbol','ioc'):
            return

        if not isinstance(params.get('value'), six.string_types):
            raise marshmallow.ValidationError(
                "'value' must hold a valid name",'value')

        if params.get('type') == 'symbol'\
        and not is_valid_identifier(params.get('value')):
            raise marshmallow.ValidationError(
                "'value' must hold a valid name",'value')


class ArgumentDependencyAdapter(BaseDependencyAdapter):
    type = marshmallow.fields.String(choices=['literal','symbol','ioc'], required=True)
    value = marshmallow.fields.Field(required=True)


class CallableDependencyAdapter(BaseDependencyAdapter):
    type = marshmallow.fields.String(choices=['symbol','ioc'], required=True)
    value = marshmallow.fields.Field(required=True)


class SymbolDependencyAdapter(marshmallow.Schema):
    type = marshmallow.fields.String(choices=['symbol'], required=True)
    name = marshmallow.fields.String(required=True)
    value = marshmallow.fields.String(required=True)
    visibility = marshmallow.fields.String(missing='public', choices=['public','internal'])
    invoke = marshmallow.fields.Boolean(load_from='callable', missing=False)
    args = marshmallow.fields.List(marshmallow.fields.Field, missing=list)
    kwargs = marshmallow.fields.Dict(marshmallow.fields.Field, missing=dict)
    tags = marshmallow.fields.List(
        marshmallow.fields.String,
        required=False,
        missing=list
    )

    @marshmallow.post_load
    def resolve_symbol(self, params):
        symbol = params['value']
        if symbol == 'bytes':
            params['value'] = 'ioc.utils.bytesequence'
        return SimpleDependency(**params)


class LiteralDependencyAdapter(marshmallow.Schema):
    type = marshmallow.fields.String(choices=['literal'])
    name = marshmallow.fields.String(required=True)
    value = marshmallow.fields.Field(required=True)
    visibility = marshmallow.fields.String(missing='public', choices=['public','internal'])
    tags = marshmallow.fields.List(
        marshmallow.fields.String,
        required=False,
        missing=list
    )

    @marshmallow.post_load
    def resolve(self, params):
        return LiteralDependency(**params)


class NestedDependencyAdapter(marshmallow.Schema):
    name = marshmallow.fields.String(required=True)
    visibility = marshmallow.fields.String(missing='public', choices=['public','internal'])
    factory = marshmallow.fields.Nested(CallableDependencyAdapter, required=True)
    args = marshmallow.fields.List(
        marshmallow.fields.Nested(ArgumentDependencyAdapter),
        required=False,
        missing=list
    )
    kwargs = marshmallow.fields.Dict(
        marshmallow.fields.Nested(ArgumentDependencyAdapter),
        required=False,
        missing=dict
    )
    chain = marshmallow.fields.List(
        marshmallow.fields.Nested(ArgumentDependencyAdapter),
        required=False,
        missing=list
    )
    tags = marshmallow.fields.List(
        marshmallow.fields.String,
        required=False,
        missing=list
    )

    @marshmallow.post_load
    def resolve(self, params):
        if params.get('kwargs'):
            params['kwargs'] = {x: SchemaRequirement(**y)
                for x,y in params.pop('kwargs').items()}

        return NestedDependency(**params)


class DependencyCollectionAdapter(marshmallow.Schema):
    name = marshmallow.fields.String(required=True)
    members = marshmallow.fields.List(
        marshmallow.fields.String,
        required=True
    )

    @marshmallow.post_load
    def resolve(self, params):
        return DependencyCollection(**params)

