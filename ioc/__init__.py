import glob
import itertools

import yaml

from ioc import provider
from ioc import schema
from ioc.requirement import DeclaredRequirement
from ioc.requirement import NOT_PROVIDED
from ioc.requirement import NO_DEFAULT
from ioc.exc import UnsatisfiedDependency


def require(names, *args, **kwargs):
    return DeclaredRequirement(provider, names, *args, **kwargs)


def provide(name, value, force=False, tags=None):
    """Register a Python object as a dependency under the key `name`."""
    provider.register(name, value, force=force, tags=tags)


def override(name, value):
    return provide(name, value, force=True)


def teardown():
    """Tear down the configured dependencies."""
    provider.teardown()


def load(dependencies, override=False):
    s = schema.Parser(override=override).load(dependencies)
    s.resolve(schema.Resolver())


def load_config(filenames, override=False):
    for filepath in itertools.chain(*[glob.glob(x) for x in filenames]):
        with open(filepath) as f:
            load(yaml.safe_load(f.read()), override=override)


def is_satisfied(name):
    return provider.is_satisfied(name)


def tagged(tag):
    return provider.tagged(tag)



class class_property(object):

    def __init__(self, name, factory=None, default=NO_DEFAULT):
        self.name = name
        self.factory = factory or (lambda x: x)
        self.dep = require(name, default=default)
        self.default = default

    def __get__(self, obj, objtype):
        if self.dep._injected is NOT_PROVIDED:
            self.dep._setup()
        return self.factory(self.dep)
