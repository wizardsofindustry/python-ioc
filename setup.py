#!/usr/bin/env python3
from distutils.core import setup


setup(
    name='python-ioc',
    version='1.2.10',
    description='Python Inversion of Control Framework',
    author='Cochise Ruhulessin',
    author_email='cochiseruhulessin@gmail.com',
    url='https://www.wizardsofindustry.net',
    project_name='Inversion of Control',
    install_requires=[
        'marshmallow',
        'PyYAML',
        'six'
    ],
    packages=[
        'ioc',
        'ioc.schema',
    ]
)
