.. _overview::

===============================
Inversion of Control for Python
===============================


Overview
========


Quickstart
==========


.. code:: python

    import ioc

    class OrderService:
        fraud_service = ioc.require('FraudDetectionService')
        repository = ioc.require('OrderRepository')

        def place_order(self, order_id):
            order = self.repository.get(order_id=order_id)

            # .. code
            if not self.fraud_service.allow_transactions(order.customer_id):
                self.reject(order_id)
            # .. code

        def reject(self, order_id):
            order = self.repository.get(order_id=order_id)
            order.reject()
            self.repository.persist(order)
            return


The code example is straightforward: if the fraud detection service flags
a customer, its order is to be rejected.

The ``OrderService`` has two external dependencies: the ``OrderRepository``
(used to get the state of an order from the persistence layer) and the
``FraudDetectionService``, which exposes an interface to do fraud-checks
on placed orders.

These dependencies are not imported directly. Instead, they are wired into
the application using the ``ioc.require()`` method. This has the following
advantages:

-   The code for the ``OrderService`` does not depend on the libraries on which
    the ``OrderRepository`` and ``FraudDetectionService`` depend.
-   The ``OrderRepository`` and ``FraudDetectionService`` may be mocked on a very
    granular level during unit tests.
-   The dependency configuration for the application may be changed without
    restarting the process.


The dependency configuration schema
===================================

-   Check if there are no duplicate dependencies.


Dependency types
================
There are three types of dependencies.

``LiteralDependency``
---------------------
A ``LiteralDependency`` is a dependency that is injected as-is from the
configuration file parser.

A ``LiteralDependency`` is declared using the following schema:

.. code:: yaml

  name: <string>
  visibility: <public|internal>
  value: <any>


``SimpleDependency``
--------------------
The ``SimpleDependency`` is a dependency that points directly to a Python
symbol, such as a module, variable, function, class or classmethod.

A ``SimpleDependency`` may be declared using the following schema:

::

  name: <string>
  visibility: <public|internal>
  type: <module|symbol|classmethod>
  value: <string>
  callable: <boolean>
  args: <list>
  kwargs: <dict>


If ``callable`` is ``true``, the dependency MUST be callable and will be
invoked using the provided ``args`` and ``kwargs`` (which may be an empty
list and/or dictionary if the callable is invoked without any arguments).
The callable is invoked immediately when the object is parsed from the
configuration file.


``NestedDependency``
--------------------
A ``NestedDependency`` is a type of dependency that may refer to other
dependencies known by the ``ioc`` module to construct itself.

A ``NestedDependency`` is declared using the following schema:

::

  name: <string>
  visibility: <public|internal>
  factory:
    type: <symbol|ioc>
    value: <string>
  args:
  - type: <literal|symbol|ioc>
    value: <any>
  kwargs:
    <string>:
    type: <literal|symbol|ioc>
    value: <any>
  chain:
  - type: <symbol|ioc>
    value: <string>


The ``factory`` parameter specifies the factory that is used to construct
the dependency, using the given positional arguments ``args`` and keyword
arguments ``kwargs``.

The ``chain`` parameter specifies a list of callables that are invoked
using the output of the former.


Examples
========


Setup an SQLAlchemy scoped session
----------------------------------
The example below creates an ``sqlalchemy`` engine, session and
wraps using the ``scoped_session()`` function.

::

  name: SQLALCHEMY_CONNECTION_DSN
  value: "sqlite://"

  # Equivalent to create_engine(dsn, echo=True)
  name: sqlalchemy.engine
  factory:
    type: symbol
    value: sqlalchemy.create_engine
  args:
  - type: ioc
    value: SQLALCHEMY_CONNECTION_DSN
  kwargs:
    echo:
      type: literal
      value: true

  # Equivalent to scoped_session(sessionmaker(bind=engine))
  name: database.session
  factory:
    type: symbol
    value: sqlalchemy.orm.sessionmaker
  kwargs:
    bind:
      type: ioc
      value: sqlalchemy.engine
  chain:
  - type: symbol
    value: sqlalchemy.orm.scoped_session


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

